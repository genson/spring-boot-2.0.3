package com.lee.register.filter;

import javax.servlet.*;
import java.io.IOException;

public class MyFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("myfilter init...");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("过滤处理");
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        System.out.println("myfilter destroy...");
    }
}
