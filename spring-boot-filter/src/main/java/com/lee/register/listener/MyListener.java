package com.lee.register.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MyListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("mylistener init...");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("mylistener destroyed...");
    }
}
