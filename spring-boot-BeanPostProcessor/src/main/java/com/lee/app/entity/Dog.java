package com.lee.app.entity;

public class Dog {

    private String name;

    public Dog(String name) {
        this.name = name;
        System.out.println("Dog 实例化......");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
