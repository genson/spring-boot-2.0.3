package com.lee.app.configuration;

import com.lee.app.entity.Cat;
import com.lee.app.entity.Dog;
import com.lee.app.entity.Pig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AnimalConfig {

    public AnimalConfig() {
        System.out.println("AnimalConfig 实例化");
    }

    @Bean
    public Dog dog() {
        return new Dog("小七");
    }

    @Bean
    public Cat cat() {
        return new Cat("Tom");
    }

    @Bean
    public Pig pig() {
        return new Pig("佩奇");
    }
}
