package com.lee.app.service.impl;

import com.lee.app.entity.Cat;
import com.lee.app.entity.Dog;
import com.lee.app.entity.Pig;
import com.lee.app.service.IAnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.inject.Inject;

@Service
public class AnimalServiceImpl implements IAnimalService {

    @Autowired
    private Dog dog;
    @Resource
    private Cat cat;
    @Inject
    private Pig pig;

    @Override
    public void printName() {
        System.out.println(dog.getName());
        System.out.println(cat.getName());
        System.out.println(pig.getName());
    }
}
