package com.lee.cache.web;

import com.lee.cache.model.User;
import com.lee.cache.service.ICacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class CacheController {

    @Autowired
    private ICacheService cacheService;

    @RequestMapping("/getName")
    @ResponseBody
    public String getName() {
        System.out.println("CacheController getName");
        return cacheService.getName();
    }

    @RequestMapping("/getUserName")
    @ResponseBody
    public String getUserName() {
        System.out.println("CacheController getUserName");
        return cacheService.getUserName();
    }

    @RequestMapping("/listUser")
    @ResponseBody
    public List<User> listUser() {
        System.out.println("CacheController listUser");
        return cacheService.listUser(2, 5);
    }
}
