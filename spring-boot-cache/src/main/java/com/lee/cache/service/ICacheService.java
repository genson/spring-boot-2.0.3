package com.lee.cache.service;

import com.lee.cache.model.User;

import java.util.List;

public interface ICacheService {

    String getName();

    String getUserName();

    List<User> listUser(int pageNum, int pageSize);
}
