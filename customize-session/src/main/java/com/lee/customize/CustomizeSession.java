package com.lee.customize;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import java.util.*;

public class CustomizeSession implements HttpSession {

    private final Map<String, Object> attributeMap = new LinkedHashMap<>();
    private boolean isNew;
    private String id;

    public CustomizeSession(String id) {
        if (id == null || "".equals(id.trim())) {
            id = UUID.randomUUID().toString().replace("-", "");
        }
        this.id = id;
        this.isNew = true;
        System.out.println("新session id : " + this.id);
    }

    public long getCreationTime() {
        return 0;
    }

    public String getId() {
        return this.id;
    }

    public long getLastAccessedTime() {
        return 0;
    }

    public ServletContext getServletContext() {
        return null;
    }

    public void setMaxInactiveInterval(int interval) {

    }

    public int getMaxInactiveInterval() {
        return 0;
    }

    public HttpSessionContext getSessionContext() {
        return null;
    }

    public Object getAttribute(String name) {
        return this.attributeMap.get(name);
    }

    public Object getValue(String name) {
        return null;
    }

    public Enumeration getAttributeNames() {
        System.out.println("CustomizeSession的getAttributeNames方法");
        return Collections.enumeration(this.attributeMap.keySet());
    }

    public String[] getValueNames() {
        return new String[0];
    }

    public void setAttribute(String name, Object value) {
        this.attributeMap.put(name, value);
    }

    public void putValue(String name, Object value) {

    }

    public void removeAttribute(String name) {

    }

    public void removeValue(String name) {

    }

    public void invalidate() {

    }

    public boolean isNew() {
        return this.isNew;
    }

    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }
}
