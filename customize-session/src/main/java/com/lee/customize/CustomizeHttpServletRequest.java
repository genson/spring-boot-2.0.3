package com.lee.customize;

import javax.servlet.http.*;

public class CustomizeHttpServletRequest extends HttpServletRequestWrapper {

    private HttpServletResponse response;

    public CustomizeHttpServletRequest(HttpServletRequest request, HttpServletResponse response) {
        super(request);
        this.response = response;
    }

    @Override
    public HttpSession getSession() {

        //return super.getSession(); // 这样就是沿用servlet容器的session管理

        return this.getSession(true);
    }

    @Override
    public HttpSession getSession(boolean create) {

        Cookie[] cookies = this.getCookies();
        String sessionId = "";

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (CustomizeSessionContainer.DEFAULT_SESSION_ID_NAME.equals(cookie.getName())) {
                    sessionId = cookie.getValue();
                    break;
                }
            }
        }

        HttpSession customizeSession = CustomizeSessionContainer.getSession(sessionId, create, response);
        return customizeSession;
    }
}
