springboot2.0.3：框架基础

模版：thymeleaf，实现国际化  
　　html加入xmlns:th="http://www.thymeleaf.org 使得html支持thymeleaf标签  
  
一般会有三种资源文件  
　　默认：message.properties， 当找不到语言配置的时候用该资源  
　　英文：messages_en_US.properties  
　　中文：messages_zh_CN.properties