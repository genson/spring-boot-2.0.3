package com.lee.quartz.mapper;

import com.lee.quartz.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {

    int saveUser(User user);
}
