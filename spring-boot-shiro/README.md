springboot：框架基础

安全框架：shiro  
　　salt的取值，密码的生成：MD5EncryptionUtil  
　　redis做shiro缓存  
　　　　缓存的更新、删除；刷新缓存有效时间等需要手动去更新，shiro不会自动更新
　　rememberMe, 需要序列化用户对象
    
ORM：mybatis  
　　数据库连接池：druid  
　　　　druid的监控配置可以配置化

日志实现：slf4j + logback  

ulisesbocchio：敏感信息的加密于解密