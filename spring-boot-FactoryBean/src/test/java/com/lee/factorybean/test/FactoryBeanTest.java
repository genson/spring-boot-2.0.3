package com.lee.factorybean.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FactoryBeanTest {

    @Autowired
    private BeanFactory beanFactory;

    @Test
    public void test() {
        Object user = beanFactory.getBean("&user");
        System.out.println(user.getClass().getName());
    }

}
