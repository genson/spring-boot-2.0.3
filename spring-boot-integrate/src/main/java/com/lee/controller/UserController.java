package com.lee.controller;

import com.github.pagehelper.PageInfo;
import com.lee.entity.User;
import com.lee.service.IUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private IUserService userService;

    @RequestMapping("/findUserByUsername")
    @ResponseBody
    public User findUserByUsername(String username) {
        LOGGER.info("username = {}", username);
        User user = userService.findUserByUsername(username);
        return user;
    }

    @RequestMapping("/userList")
    @RequiresPermissions("user:view")
    public String userList(int pageNum, int pageSize, Model model) {
        LOGGER.info("pageNum = {}, pageSize = {}", pageNum, pageSize);
        PageInfo pageInfo = userService.listUser(pageNum, pageSize);
        model.addAttribute("pageInfo", pageInfo);

        return "user/userList";
    }

    /**
     * 用户添加;
     * @return
     */
    @RequestMapping("/userAdd")
    @RequiresPermissions("user:add")
    public String userAdd(){
        return "user/userAdd";
    }

    /**
     * 用户删除;
     * @return
     */
    @RequestMapping("/userDel")
    @RequiresPermissions("user:del")
    public String userDel(){
        return "user/userDel";
    }
}
