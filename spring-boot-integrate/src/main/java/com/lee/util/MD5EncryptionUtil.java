package com.lee.util;

import org.apache.shiro.crypto.hash.SimpleHash;

import java.util.UUID;

public class MD5EncryptionUtil {

    /**
     *  根据用户名随机生成盐值
     * @param username 用户名
     * @return
     */
    public static String getSalt(String username) {
        String salt = UUID.nameUUIDFromBytes(username.getBytes()).toString().replaceAll("-", "");
        return salt;
    }

    /**
     * 对原始密码进行加密
     * @param originalPassword
     * @param salt
     * @param username
     * @return
     */
    public static String encryptionPwd(String originalPassword, String salt, String username, String hashAlgorithm, int hashIteration) {
        SimpleHash newPassword = new SimpleHash(hashAlgorithm,originalPassword,username+salt,hashIteration);
        return newPassword.toString();
    }
}
