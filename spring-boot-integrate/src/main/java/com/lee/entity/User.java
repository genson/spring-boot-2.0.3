package com.lee.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 用户名
     */
    private String username;
    /**
     * 登录密码
     */
    private String password;
    /**
     * 盐，用于密码加密
     */
    private String salt;
    /**
     * 用户状态，1：可用，0：不可用
     */
    private Integer state;
    /**
     * 描述
     */
    private String description;

    /**
     * 密码盐.
     * @return
     */
    public String getCredentialsSalt(){

        return this.username+this.salt;
    }
}
