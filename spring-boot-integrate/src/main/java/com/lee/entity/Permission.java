package com.lee.entity;

import lombok.Data;

@Data
public class Permission {
    private Long id;
    private String name;
    private String permission;
    private String url;
    private String description;
}
