package com.lee.entity;

import lombok.Data;

@Data
public class Role {
    private Long id;
    private String name;
    private String description;
}
