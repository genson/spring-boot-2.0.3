springboot：框架基础

安全框架：shiro  
　　salt的取值，密码的生成：MD5EncryptionUtil  
　　redis做shiro缓存  
　　　　缓存的更新、删除；刷新缓存有效时间等需要手动去更新，shiro不会自动更新  
　　rememberMe, 需要序列化用户对象  
　　authorization、session做缓存处理，authentication不做缓存处理
    
ORM：mybatis  
　　数据库连接池：druid  
　　　　druid的监控配置可以配置化

日志实现：slf4j + logback

模版：thymeleaf，实现国际化  
　　html加入xmlns:th="http://www.thymeleaf.org 使得html支持thymeleaf标签

lombok：样板式代码,消除冗余代码    
　　lombok注解问题，idea中安装lombok插件,重启idea即可  

ulisesbocchio：敏感信息的加密于解密  

注意点：  
　　开发的时候可以用内嵌的容器，但是部署的时候最好别用内嵌容器，特别是读文件的时候会出现找不到的问题  
　　部署此项目需要tomcat8及以上版本，有些注解tomcat8以下版本不支持，比如lombak，会报Unable to process Jar entry [module-info.class] from Jar类似的错