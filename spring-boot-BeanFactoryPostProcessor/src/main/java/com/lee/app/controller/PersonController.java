package com.lee.app.controller;

import com.github.pagehelper.PageInfo;
import com.lee.app.entity.Person;
import com.lee.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("person")
public class PersonController {

    @Autowired
    private IPersonService personService;

    @RequestMapping(value = "list", method = RequestMethod.POST)
    @ResponseBody
    public PageInfo<Person> listPerson(int pageSize, int pageNum) {
        return personService.listPerson(pageSize, pageNum);
    }

}
