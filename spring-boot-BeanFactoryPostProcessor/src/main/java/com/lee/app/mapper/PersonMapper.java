package com.lee.app.mapper;

import com.lee.app.entity.Person;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PersonMapper {

    List<Person> listPerson();

}
