package com.lee.service;

import com.github.pagehelper.PageInfo;
import com.lee.app.entity.Person;

public interface IPersonService {

    PageInfo<Person> listPerson(int pageSize, int pageNum);

}
