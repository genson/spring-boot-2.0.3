package com.lee.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lee.app.entity.Person;
import com.lee.app.mapper.PersonMapper;
import com.lee.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements IPersonService {

    @Autowired
    private PersonMapper personMapper;

    @Override
    public PageInfo<Person> listPerson(int pageSize, int pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        List<Person> peoples = personMapper.listPerson();
        PageInfo<Person> pageInfo = new PageInfo<>(peoples);
        return pageInfo;
    }
}
