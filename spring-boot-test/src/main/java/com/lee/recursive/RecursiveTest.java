package com.lee.recursive;

public class RecursiveTest {

    public static void main(String[] args) {
        Person p = new Person();
        p.setName("李四");
        p.setPerson(p);
        System.out.println(p);
    }
}

class Person {
    private String name;
    private Person person;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", person=" + person +
                '}';
    }
}
