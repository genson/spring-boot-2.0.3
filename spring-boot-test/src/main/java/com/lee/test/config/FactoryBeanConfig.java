package com.lee.test.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FactoryBeanConfig {

    @Bean
    public MyFactoryBean myFactoryBean() {
        MyFactoryBean myFactoryBean = new MyFactoryBean();
        return myFactoryBean;
    }
}
