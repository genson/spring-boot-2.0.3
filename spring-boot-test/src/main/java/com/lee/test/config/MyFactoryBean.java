package com.lee.test.config;

import org.springframework.beans.factory.FactoryBean;

public class MyFactoryBean implements FactoryBean {

    public MyFactoryBean() {
        System.out.println("MyFactoryBean 构造方法");
    }

    @Override
    public Object getObject() throws Exception {
        return new MyBean();
    }

    @Override
    public Class getObjectType() {
        return MyBean.class;
    }

    private static final class MyBean {

        private String name;

        protected MyBean () {
            this.name = "bing";
            System.out.println("MyBean 构造方法");
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
