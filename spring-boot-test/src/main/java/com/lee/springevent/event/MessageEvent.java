package com.lee.springevent.event;

import org.springframework.context.ApplicationEvent;

/**
 * 短信事件，事件信息的载体
 * 可以从中获取事件源信息，本例中source不代表事件源
 */
public class MessageEvent extends ApplicationEvent {

    public MessageEvent(Object source) {
        super(source);
    }
}
