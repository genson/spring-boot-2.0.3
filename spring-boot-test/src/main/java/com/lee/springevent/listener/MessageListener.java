package com.lee.springevent.listener;

import com.lee.springevent.event.MessageEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

public class MessageListener implements ApplicationListener{

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if(event instanceof MessageEvent) {
            String phoneNumber = (String)event.getSource();
            System.out.println("我已收到通知：即将向"+phoneNumber+"发短信了...");
        }
    }
}
