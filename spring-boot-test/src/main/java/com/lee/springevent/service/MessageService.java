package com.lee.springevent.service;

import com.lee.springevent.event.MessageEvent;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * ApplicationContextAware，能够获取spring的上下文环境
 *
 * MessageService相当于之前说的事件环境
 */
public class MessageService implements ApplicationContextAware {

    private ApplicationContext ctx;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }

    public void sendMessage(String phoneNumber)
    {
        MessageEvent evt = new MessageEvent(phoneNumber);

        // 发布事件
        ctx.publishEvent(evt);
    }
}
