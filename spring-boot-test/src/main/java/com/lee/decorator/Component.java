package com.lee.decorator;

public interface Component {

    void sampleOperation();
}
