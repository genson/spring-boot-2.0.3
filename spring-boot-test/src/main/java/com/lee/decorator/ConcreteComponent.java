package com.lee.decorator;

public class ConcreteComponent implements Component {

    @Override
    public void sampleOperation() {
        // 写具体业务代码
        System.out.println("我是ConcreteComponent");
    }
}
