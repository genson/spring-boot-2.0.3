package com.lee.decorator;

public class Decorator implements Component {

    private Component component;

    public Decorator(Component component) {
        this.component = component;
    }

    @Override
    public void sampleOperation() {

        // 委派给具体的构建
        component.sampleOperation();
    }
}
