package com.lee.decorator;

public class ConcreteDecorator extends Decorator{

    public ConcreteDecorator(Component component) {
        super(component);
    }

    @Override
    public void sampleOperation() {
        // 写相关的业务代码
        System.out.println("调用component方法之前业务处理");

        super.sampleOperation();

        // 写相关的业务代码
        System.out.println("调用component方法之后业务处理");
    }
}
