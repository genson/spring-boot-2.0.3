package com.lee.concurrent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public interface Future<V> {

    /**
     * 取消任务，如果取消任务成功则返回true，如果取消任务失败则返回false
     * @param mayInterruptIfRunning 是否允许取消正在执行却没有执行完毕的任务，如果设置true，则表示可以取消正在执行过程中的任务。
     *         如果任务已经完成，则无论mayInterruptIfRunning为true还是false，此方法肯定返回false，即如果取消已经完成的任务会返回false。
     *         如果任务正在执行，若mayInterruptIfRunning设置为true，则返回true，若mayInterruptIfRunning设置为false，则返回false。
     *         如果任务还没有执行，则无论mayInterruptIfRunning为true还是false，肯定返回true。
     * @return
     */
    boolean cancel(boolean mayInterruptIfRunning);

    /**
     * 任务是否被取消成功，如果在任务正常完成前被取消成功，则返回true。
     * @return
     */
    boolean isCancelled();

    /**
     * 任务是否已经完成，若完成则返回true。
     * @return
     */
    boolean isDone();

    /**
     * 获取执行结果，这个方法会产生阻塞，会一直等到任务执行完毕才返回。
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     */
    V get() throws InterruptedException, ExecutionException;

    /**
     * 获取执行结果，如果在指定时间内没获取到结果，就直接返回null
     * @param timeout
     * @param unit
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     * @throws TimeoutException
     */
    V get(long timeout, TimeUnit unit)
            throws InterruptedException, ExecutionException, TimeoutException;
}