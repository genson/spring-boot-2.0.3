package com.lee.concurrent;

import java.util.concurrent.*;
import java.util.concurrent.Future;

public class RunnableTest {

    public static void main(String[] args) throws InterruptedException, ExecutionException, TimeoutException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        MyRunnable myRunnable = new MyRunnable();

        // 为了可取消性而使用Future但又不提供可用的结果，则可以声明 Future<?> 形式类型、并返回null作为底层任务的结果
        Future<?> submit = executorService.submit(myRunnable);

        // 如果不shutdown，那它里面的线程会一直处于运行状态，应用不会停止
        executorService.shutdown();

        // 输出任务执行结果，由于Runnable没有返回值，所以get的是null
        System.out.println(submit.get(4, TimeUnit.SECONDS));
        System.out.println("我是主线程...");
    }
}
class MyRunnable implements Runnable {

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("我是子线程1");
    }
}