package com.lee.concurrent;

import java.util.concurrent.*;

public class CallableTest {

    public static void main(String[] args) throws InterruptedException, ExecutionException, TimeoutException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        Task task = new Task();
        FutureTask<String> futureTask = new FutureTask<>(task);
        executorService.submit(futureTask);
        executorService.shutdown();

        Thread.sleep(1000);
        System.out.println("我是主线程, 执行另外的业务...");

        System.out.println("task执行结果：" + futureTask.get(5, TimeUnit.SECONDS));

        System.out.println("任务全部执行完毕...");
    }
}
class Task implements Callable<String> {

    @Override
    public String call() throws Exception {
        System.out.println("子线程, 业务处理中...");
        Thread.sleep(2000);
        return "业务执行成功";
    }
}
