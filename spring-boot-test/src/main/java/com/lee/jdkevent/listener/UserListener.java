package com.lee.jdkevent.listener;

import com.lee.jdkevent.event.UserEvent;

import java.util.EventListener;

/**
 * 用户监听器
 * @param <E>
 */
public interface UserListener<E extends UserEvent> extends EventListener {

    void onRegister(E event);
}
