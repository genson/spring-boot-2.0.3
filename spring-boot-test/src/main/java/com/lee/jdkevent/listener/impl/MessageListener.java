package com.lee.jdkevent.listener.impl;

import com.lee.jdkevent.event.MessageEvent;
import com.lee.jdkevent.event.UserEvent;
import com.lee.jdkevent.listener.UserListener;
import com.lee.jdkevent.model.User;

/**
 * 具体用户监听器：发送短信监听器
 */
public class MessageListener implements UserListener {

    @Override
    public void onRegister(UserEvent event) {
        if (event instanceof MessageEvent) {

            Object source = event.getSource();
            User user = (User) source;
            System.out.println("send message to " + user.getPhoneNumber());
        }
    }
}
