package com.lee.test;

import com.lee.myobserver.Observer;
import com.lee.myobserver.impl.ConcreteObserver;
import com.lee.myobserver.impl.ConcreteSubject;

public class MyObserverTest {

    public static void main(String[] args) {
        ConcreteSubject subject = new ConcreteSubject();
        Observer observer = new ConcreteObserver();
        subject.attach(observer);
        subject.changeState("new state");
    }
}
