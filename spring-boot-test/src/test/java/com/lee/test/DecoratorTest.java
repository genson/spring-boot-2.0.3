package com.lee.test;

import com.lee.decorator.Component;
import com.lee.decorator.ConcreteComponent;
import com.lee.decorator.ConcreteDecorator;
import com.lee.decorator.Decorator;

public class DecoratorTest {

    public static void main(String[] args) {
        Component component = new ConcreteComponent();
        Decorator decorator = new ConcreteDecorator(component);
        decorator.sampleOperation();
    }
}
