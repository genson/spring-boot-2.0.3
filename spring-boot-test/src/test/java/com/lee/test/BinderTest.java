package com.lee.test;

import com.lee.binder.model.Hello;
import com.lee.springevent.service.MessageService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class BinderTest {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(BinderTest.class, args);
        Environment environment = context.getEnvironment();
        Hello hello = new Hello();
        Hello hello1 = Binder.get(environment).bind("my.hello", Bindable.ofInstance(hello)).get();
        hello1.getName();
    }
}
