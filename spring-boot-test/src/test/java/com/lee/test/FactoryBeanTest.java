package com.lee.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
public class FactoryBeanTest {

    @Autowired
    private ApplicationContext applicationContext;


    @Test
    public void testMyFactoryBean() throws Exception {
        Object myFactoryBean = applicationContext.getBean("myFactoryBean");

        System.out.println(myFactoryBean.getClass().getName());
    }
}
