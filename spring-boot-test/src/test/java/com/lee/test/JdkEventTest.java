package com.lee.test;

import com.lee.jdkevent.listener.impl.MessageListener;
import com.lee.jdkevent.model.User;
import com.lee.jdkevent.service.UserService;

public class JdkEventTest {

    public static void main(String[] args) {

        UserService service = new UserService();

        // 添加发送短信监听器
        service.addListeners(new MessageListener());
        // 还可以添加其他监听器...

        User user = new User("zhangsan", "123456", "15174480311");
        service.register(user);
    }
}
