package com.lee.condition.model;

public class Dog {

    private String name;

    public Dog() {}

    public Dog(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
