package com.lee.condition.web;

import com.lee.condition.model.Cat;
import com.lee.condition.model.Dog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/condition")
public class ConditionWeb {

    @Autowired
    private Cat cat;
    @Autowired
    private Dog dog;

    @RequestMapping("/getCat")
    public Cat getCat() {
        System.out.println("get cat...");
        return cat;
    }

    @RequestMapping("/getDog")
    public Dog getDog() {
        System.out.println("get dog...");
        return dog;
    }
}
