package com.lee.condition.config;

import com.lee.condition.model.Cat;
import com.lee.condition.model.Dog;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConfiguration {

    @Bean
    public Cat myCat() {
        return new Cat("myCat");
    }

    @Bean
    @ConditionalOnBean(Cat.class)       // @ConditionalOnBean可以换成其他的
    public Dog myDog() {
        return new Dog("myDog");
    }
}