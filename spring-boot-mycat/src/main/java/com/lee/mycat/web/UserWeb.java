package com.lee.mycat.web;

import com.lee.mycat.entity.User;
import com.lee.mycat.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mycat")
public class UserWeb {

    @Autowired
    private IUserService userService;

    @RequestMapping("/getUserByNameFromMasterDb")
    public User getUserByNameFromMasterDb(String name) {
        return userService.getUserByNameFromMasterDb(name);
    }

    @RequestMapping("/getUserByNameFromSlaveDb")
    public User getUserByNameFromSlaveDb(String name) {
        return userService.getUserByNameFromSlaveDb(name);
    }

    @RequestMapping("/getUserByName")
    public User getUserByName(String name) {
        return userService.getUserByName(name);
    }

    @RequestMapping("/addUser")
    public Integer addUser(String name, Integer age) {
        return userService.insertUser(new User(name, age));
    }
}
