package com.lee.mycat.service;

import com.lee.mycat.entity.User;

public interface IUserService {

    /**
     * 强制从master db获取数据
     * @param name
     * @return
     */
    User getUserByNameFromMasterDb(String name);

    /**
     * 强制从slave db获取数据
     * @param name
     * @return
     */
    User getUserByNameFromSlaveDb(String name);

    /**
     * 一般查询，从slave db获取数据
     * @param name
     * @return
     */
    User getUserByName(String name);

    int insertUser(User user);
}
