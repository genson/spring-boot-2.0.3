package com.lee.mycat.service.impl;

import com.lee.mycat.entity.User;
import com.lee.mycat.mapper.UserMapper;
import com.lee.mycat.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User getUserByNameFromMasterDb(String name) {
        return userMapper.getUserByNameFromMasterDb(name);
    }

    @Override
    public User getUserByNameFromSlaveDb(String name) {
        return userMapper.getUserByNameFromSlaveDb(name);
    }

    @Override
    public User getUserByName(String name) {
        return userMapper.getUserByName(name);
    }

    @Override
    public int insertUser(User user) {
        return userMapper.insertUser(user);
    }
}
