package com.lee.mycat.mapper;

import com.lee.mycat.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {

    /**
     * 强制从master db获取数据
     * @param name
     * @return
     */
    User getUserByNameFromMasterDb(String name);

    /**
     * 强制从slave db获取数据
     * @param name
     * @return
     */
    User getUserByNameFromSlaveDb(String name);

    /**
     * 一般查询，从slave db获取数据
     * @param name
     * @return
     */
    User getUserByName(String name);

    /**
     * 新增user
     * @param user
     * @return
     */
    int insertUser(User user);
}
