http://localhost:8886/mycat/addUser?name=Jiraiye&age=50
    添加一个用户
http://localhost:8886/mycat/getUserByName?name=Jiraiye
    一般情况下的查询，此时请求会走mysql slave
http://localhost:8886/mycat/getUserByNameFromMasterDb?name=Jiraiye
    指定查询mysql master，此时请求会走mysql master
http://localhost:8886/mycat/getUserByNameFromSlaveDb?name=Jiraiye
    指定查询mysql slave，此时请求会走mysql slave