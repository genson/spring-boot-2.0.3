自动配置示例
    spring4.2之后，支持@Configuration修饰的类、ImportSelector实现类、ImportBeanDefinitionRegistrar实现类、常规的java类
    @Configuration修饰的类：AnimalConfig
    ImportSelector实现类：RoleImportSelector
    ImportBeanDefinitionRegistrar实现类：PermissionImportBeanDefinitionRegistrar
    常规java类：User

    @ImportResource可以导入xml形式的配置