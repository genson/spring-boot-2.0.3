package com.lee.autoconfig.config;

import com.lee.autoconfig.entity.User;
import org.springframework.context.annotation.Import;

@Import({AnimalConfig.class, RoleImportSelector.class, PermissionImportBeanDefinitionRegistrar.class, User.class})
public class ImportAutoConfig {
}
