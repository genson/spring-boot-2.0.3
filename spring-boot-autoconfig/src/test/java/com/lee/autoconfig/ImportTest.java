package com.lee.autoconfig;

import com.lee.autoconfig.config.ImportAutoConfig;
import com.lee.autoconfig.entity.XmlBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ImportTest {

    @Autowired
    private XmlBean xmlBean;

    @Test
    public void test() {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(ImportAutoConfig.class);
        String[] beanNames = applicationContext.getBeanDefinitionNames();  // 此种方式获取不到xml中的bean定义
        for(int i=0;i<beanNames.length;i++){
            System.out.println("beanName = " + beanNames[i]);
        }
        System.out.println("xmlBean实力是否为空" + xmlBean == null);
    }
}
